package datasource

import "lmwn-assignment/elasticsearch"

// Datasource : datasource model for inject to repository
type Datasource struct {
	Elasticsearch elasticsearch.Interface
}
