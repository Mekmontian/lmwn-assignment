package model

// BaseResponse : normal response just return message string
type BaseResponse struct {
	Msg string `json:"message"`
	Err string `json:"error,omitempty"`
}
