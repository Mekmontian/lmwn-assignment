package model

// UpdateInterface : use for remove meta data field before update
type UpdateInterface interface {
	RemoveMetaData()
}
