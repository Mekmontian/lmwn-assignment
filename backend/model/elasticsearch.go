package model

// ElasticBasicPayload : base payload with concurrent control
type ElasticBasicPayload struct {
	Source            []byte
	ConcurrentControl *ConcurrentControl
}

// QueryWithHighlight : use for transfer data between elasticsearch and repository
type QueryWithHighlight struct {
	Source            []byte
	Highlight         []byte
	ConcurrentControl *ConcurrentControl
}

// UpdatePayload : use for send payload to update elasticsearch
type UpdatePayload struct {
	Payload           UpdateInterface
	ConcurrentControl *ConcurrentControl
}

// ConcurrentControl : model for concurrent control
type ConcurrentControl struct {
	SeqNo       int64 `json:"_seq_no"`
	PrimaryTerm int64 `json:"_primary_term"`
}

// ElasticUpdateResponse : use for updated response
type ElasticUpdateResponse struct {
	Source            []byte
	ConcurrentControl *ConcurrentControl
}
