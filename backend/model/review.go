package model

// Review : review model
type Review struct {
	ID                uint               `json:"reviewID,omitempty"`
	Review            string             `json:"review,omitempty"`
	Version           string             `json:"@version,omitempty"`
	ConcurrentControl *ConcurrentControl `json:"_concurrent_control,omitempty"`
}

// RemoveMetaData : remove meta data before update doc
func (r *Review) RemoveMetaData() {
	r.ConcurrentControl = nil
}
