package migrate

import (
	"context"
	"log"

	"github.com/olivere/elastic"
)

// Migration : migration index and mapping
func Migration(endpoint string) {
	// Connect to elasticsearch
	client, err := elastic.NewClient(
		elastic.SetURL(endpoint),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false))
	if err != nil {
		log.Fatal(err)
	}
	ctx := context.Background()
	index := "reviews_idx_1"
	alias := "reviews"
	exist, err := client.IndexExists(index).Do(ctx)
	if err != nil {
		log.Fatalf("IndexExists() ERROR: %v", err)
	} else if exist {
		log.Println("Index exists")
		return
	}
	mappings := `{
		"settings": {
			"index": {
				"number_of_shards": "3",
				"number_of_replicas": "1",
				"analysis": {
					"analyzer": {
						"food_dict": {
							"tokenizer": "standard",
							"filter": [ "15_word_decompound" ]
						}
					},
					"filter": {
						"15_word_decompound": {
							"type": "dictionary_decompounder",
							"word_list_path": "analysis/food_dictionary.txt"
						}
					}
				}
			}
		},  
		"mappings": {
			"properties": {
				"reviewID": {
					"type": "long"
				},
				"review": {
					"type": "text",
					"analyzer": "food_dict"
				}
			}
		}
	}`
	_, err = client.CreateIndex(index).Body(mappings).Do(ctx)
	if err != nil {
		log.Fatalf("Create index fail with %s", err)
	}
	_, err = elastic.NewAliasService(client).Add(index, alias).Do(ctx)
	if err != nil {
		log.Fatalf("Create alias fail with %s", err)
	}
	log.Println("Migration Succesful")
}
