package elasticsearch

import "lmwn-assignment/model"

// Interface : elasticsearch interface for query and manipulate data
type Interface interface {
	GetDocumentByID(index string, ID uint) (*model.ElasticBasicPayload, error)
	UpdateDocumentByID(index string, ID uint, data model.UpdatePayload) (*model.ElasticUpdateResponse, error)
	GetDocumentByQuery(index, field, query string) ([]model.QueryWithHighlight, error)
}
