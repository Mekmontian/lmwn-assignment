package elasticsearch_test

import (
	"lmwn-assignment/elasticsearch"
	"testing"

	"github.com/stretchr/testify/assert"
)

var es elasticsearch.Interface

func init() {
	es, _ = elasticsearch.NewElasticsearch("http://localhost:9200")
}

func TestGetDocumentByID_Success(t *testing.T) {
	res, err := es.GetDocumentByID("reviews", 1)
	if err != nil {
		t.Error(err)
	} else {
		assert.NotEqual(t, nil, res)
	}
}

func TestGetDocumentByID_NotFound(t *testing.T) {
	res, err := es.GetDocumentByID("reviews", 0)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, true, res == nil)
}

func TestGetDocumentByQuery(t *testing.T) {
	_, err := es.GetDocumentByQuery("reviews", "review", "ไก่ทอด")
	if err != nil {
		t.Error(err)
	}
}

// func TestUpdateDocumentByID(t *testing.T) {
// 	cc := model.ConcurrentControl{
// 		SeqNo:       2084,
// 		PrimaryTerm: 1,
// 	}
// 	data := model.UpdatePayload{
// 		Payload: &model.Review{
// 			Review:            "ร้านดัง  ของหวานอร่อยที่สุดก็คงต้องยกให้  after you  เลยค่า   ร้านนี้เปิดหลายสาขาแล้ว  วันนี้มาลองสาขานี้เพราะคนไม่เยอะเท่าไหร่  เมนูที่สั่งก็อร่อยเหมือนกัน  ร้านนี้อร่อยแทบทุกอย่างเลยคะ   อยากให้ลองละจะติดใจ",
// 			ConcurrentControl: &cc,
// 		},
// 		ConcurrentControl: &cc,
// 	}
// 	res, err := es.UpdateDocumentByID("reviews", 7, data)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	t.Error(res)
// }
