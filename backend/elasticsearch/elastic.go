package elasticsearch

import (
	"context"
	"errors"
	"io"
	"lmwn-assignment/constant"
	"lmwn-assignment/model"
	"strconv"

	"github.com/olivere/elastic/v7"
)

type elasticsearchImpl struct {
	client *elastic.Client
	ctx    context.Context
}

// NewElasticsearch : create elasticsearch client instance
func NewElasticsearch(endpoint string) (Interface, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(endpoint),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false))
	if err != nil {
		return nil, err
	}
	return &elasticsearchImpl{
		client: client,
		ctx:    context.Background(),
	}, nil
}

func (e *elasticsearchImpl) GetDocumentByID(index string, ID uint) (*model.ElasticBasicPayload, error) {
	searchResult, err := e.client.Get().Index(index).Id(strconv.Itoa(int(ID))).Do(e.ctx)
	if err != nil {
		if elastic.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}
	return &model.ElasticBasicPayload{
		Source: searchResult.Source,
		ConcurrentControl: &model.ConcurrentControl{
			SeqNo:       *searchResult.SeqNo,
			PrimaryTerm: *searchResult.PrimaryTerm,
		},
	}, nil
}

func (e *elasticsearchImpl) UpdateDocumentByID(index string, ID uint, data model.UpdatePayload) (*model.ElasticUpdateResponse, error) {
	data.Payload.RemoveMetaData()
	var statement *elastic.UpdateService
	statement = e.client.Update().Index(index).Id(strconv.Itoa(int(ID))).FetchSource(true)
	if data.ConcurrentControl != nil {
		statement = statement.IfSeqNo(data.ConcurrentControl.SeqNo).IfPrimaryTerm(data.ConcurrentControl.PrimaryTerm)
	}
	updateResponse, err := statement.Doc(data.Payload).Do(e.ctx)
	if err != nil {
		if elastic.IsConflict(err) {
			return nil, errors.New(constant.ErrUpdateConflict)
		}
		return nil, err
	}
	res := model.ElasticUpdateResponse{
		Source: updateResponse.GetResult.Source,
		ConcurrentControl: &model.ConcurrentControl{
			SeqNo:       updateResponse.SeqNo,
			PrimaryTerm: updateResponse.PrimaryTerm,
		},
	}
	return &res, nil
}

func (e *elasticsearchImpl) GetDocumentByQuery(index, field, query string) ([]model.QueryWithHighlight, error) {
	matchQuery := elastic.NewMatchQuery(field, query)

	highlight := elastic.NewHighlight()
	highlight = highlight.Fields(elastic.NewHighlighterField(field))
	highlight = highlight.PreTags("<keyword>").PostTags("</keyword>")

	scrollService := e.client.Scroll().Index(index).Highlight(highlight).Query(matchQuery).Size(1)

	qwhs := make([]model.QueryWithHighlight, 0)
	for {
		scrollResult, err := scrollService.Do(e.ctx)
		if err == io.EOF {
			break // all results retrieved
		}
		if err != nil {
			return []model.QueryWithHighlight{}, err // something went wrong
		}
		for _, hit := range scrollResult.Hits.Hits {
			qwh := model.QueryWithHighlight{
				Source:    hit.Source,
				Highlight: []byte(hit.Highlight[field][0]),
				// ConcurrentControl: &model.ConcurrentControl{
				// 	SeqNo:       *hit.SeqNo,
				// 	PrimaryTerm: *hit.PrimaryTerm,
				// },
			}
			qwhs = append(qwhs, qwh)
		}
	}
	return qwhs, nil
}
