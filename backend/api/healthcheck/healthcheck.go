package healthcheck

import (
	"lmwn-assignment/model"
	"net/http"

	"github.com/labstack/echo"
)

// Interface : health check interface for check server beat
type Interface interface {
	Initial(e *echo.Echo)
}

type healthCheckRoute struct {
}

// NewHealthCheckRoute : create health check route module
func NewHealthCheckRoute() Interface {
	return &healthCheckRoute{}
}

func (r *healthCheckRoute) Initial(e *echo.Echo) {
	api := e.Group("/health")
	api.GET("", healthCheck)
}

func healthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, model.BaseResponse{
		Msg: "OK",
	})
}
