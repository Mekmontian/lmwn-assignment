package handler

import (
	"lmwn-assignment/model"

	"github.com/labstack/echo"
)

// ReviewHandlerInterface : handler interface for handle api request and response it
type ReviewHandlerInterface interface {
	GetReviewByID(c echo.Context) error
	SearchReviewByQuery(c echo.Context) error
	EditReviewByID(c echo.Context) error
}

type editReviewRequestBody struct {
	Review            string                   `json:"review"`
	ConcurrentControl *model.ConcurrentControl `json:"_concurrent_control,omitempty"`
}
