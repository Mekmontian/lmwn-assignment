package handler_test

import (
	"lmwn-assignment/api/review/handler"
	"lmwn-assignment/mock/review"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

var e *echo.Echo
var h handler.ReviewHandlerInterface

func init() {
	u := review.NewMockUsecase()
	h = handler.NewReviewHandler(u)
	e = echo.New()
}

func TestGetReviewByID_Success(t *testing.T) {
	req := httptest.NewRequest(echo.GET, "/reviews", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")
	c.SetParamNames("id")
	c.SetParamValues("1")

	var reviewJSON = `{"reviewID":1,"review":"delicious","_concurrent_control":{"_seq_no":1,"_primary_term":1}}` + "\n"

	if assert.NoError(t, h.GetReviewByID(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, reviewJSON, rec.Body.String())
	}
}

func TestGetReviewByID_DismissParamID(t *testing.T) {
	req := httptest.NewRequest(echo.GET, "/reviews", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")

	var reviewJSON = `{"message":"Required parameter id"}` + "\n"

	if assert.NoError(t, h.GetReviewByID(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(t, reviewJSON, rec.Body.String())
	}
}

func TestGetReviewByID_BadParamID(t *testing.T) {
	req := httptest.NewRequest(echo.GET, "/reviews", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")
	c.SetParamNames("id")
	c.SetParamValues("400badparam")

	var reviewJSON = `{"message":"Bad parameter id"}` + "\n"

	if assert.NoError(t, h.GetReviewByID(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(t, reviewJSON, rec.Body.String())
	}
}

func TestSearchReviewByQuery_Success(t *testing.T) {
	req := httptest.NewRequest(echo.GET, "/reviews?query=delicious", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	var reviewsJSON = `[{"reviewID":1,"review":"\u003ckeyword\u003edelicious\u003c/keyword\u003e","_concurrent_control":{"_seq_no":1,"_primary_term":1}},{"reviewID":2,"review":"super \u003ckeyword\u003edelicious\u003c/keyword\u003e","_concurrent_control":{"_seq_no":1,"_primary_term":1}}]` + "\n"

	if assert.NoError(t, h.SearchReviewByQuery(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, reviewsJSON, rec.Body.String())
	}
}

func TestSearchReviewByQuery_Empty(t *testing.T) {
	req := httptest.NewRequest(echo.GET, "/reviews?query=", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	var reviewsJSON = `{"message":"Not found"}` + "\n"

	if assert.NoError(t, h.SearchReviewByQuery(c)) {
		assert.Equal(t, http.StatusNotFound, rec.Code)
		assert.Equal(t, reviewsJSON, rec.Body.String())
	}
}

func TestEditReviewByID_Success(t *testing.T) {
	body := strings.NewReader(`{"review": "bad smell"}`)
	req := httptest.NewRequest(echo.PUT, "/reviews", body)
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")
	c.SetParamNames("id")
	c.SetParamValues("1")

	var resJSON = `{"reviewID":1,"review":"new delicious","_concurrent_control":{"_seq_no":2,"_primary_term":1}}` + "\n"

	if assert.NoError(t, h.EditReviewByID(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resJSON, rec.Body.String())
	}
}

func TestEditReviewByID_DismissParamID(t *testing.T) {
	body := strings.NewReader(`{"review": "bad smell"}`)
	req := httptest.NewRequest(echo.PUT, "/reviews", body)
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")

	var resJSON = `{"message":"Required parameter id"}` + "\n"

	if assert.NoError(t, h.EditReviewByID(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(t, resJSON, rec.Body.String())
	}
}

func TestEditReviewByID_BadParamID(t *testing.T) {
	body := strings.NewReader(`{"review": "bad smell"}`)
	req := httptest.NewRequest(echo.PUT, "/reviews", body)
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")
	c.SetParamNames("id")
	c.SetParamValues("400badparam")

	var resJSON = `{"message":"Bad parameter id"}` + "\n"

	if assert.NoError(t, h.EditReviewByID(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(t, resJSON, rec.Body.String())
	}
}

func TestEditReviewByID_BadBody(t *testing.T) {
	body := strings.NewReader(`{"review": "bad smell",sadasd}`)
	req := httptest.NewRequest(echo.PUT, "/reviews", body)
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:id")
	c.SetParamNames("id")
	c.SetParamValues("1")

	if assert.NoError(t, h.EditReviewByID(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}
