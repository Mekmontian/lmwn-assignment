package handler

import (
	"lmwn-assignment/api/review/usecase"
	"lmwn-assignment/constant"
	"lmwn-assignment/model"
	"lmwn-assignment/utility"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type reviewHandler struct {
	usecase usecase.ReviewUsecaseInterface
}

// NewReviewHandler : create review handler instance
func NewReviewHandler(us usecase.ReviewUsecaseInterface) ReviewHandlerInterface {
	return &reviewHandler{usecase: us}
}

func (h *reviewHandler) GetReviewByID(c echo.Context) error {
	id := c.Param("id")
	if id != "" {
		idUint, err := strconv.ParseUint(id, 10, 64)
		if err != nil {
			return c.JSON(http.StatusBadRequest, model.BaseResponse{
				Msg: "Bad parameter id",
			})
		}
		review, err := h.usecase.GetReviewByID(uint(idUint))
		if err != nil {
			if err.Error() == constant.ErrNotFound {
				return c.JSON(http.StatusNotFound, err)
			}
			return c.JSON(http.StatusInternalServerError, err)
		}
		return c.JSON(http.StatusOK, review)

	}
	return c.JSON(http.StatusBadRequest, model.BaseResponse{
		Msg: "Required parameter id",
	})
}

func (h *reviewHandler) SearchReviewByQuery(c echo.Context) error {
	query := c.QueryParam("query")
	trimQuery := utility.TrimSpaceQuery(query)
	if query == "" {
		return c.JSON(http.StatusNotFound, model.BaseResponse{
			Msg: "Not found",
		})
	}
	reviews, err := h.usecase.SearchReviewByQuery(trimQuery)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, reviews)
}

func (h *reviewHandler) EditReviewByID(c echo.Context) error {
	id := c.Param("id")
	if id != "" {
		idUint, err := strconv.ParseUint(id, 10, 64)
		if err != nil {
			return c.JSON(http.StatusBadRequest, model.BaseResponse{
				Msg: "Bad parameter id",
			})
		}
		rb := new(editReviewRequestBody)
		if err := c.Bind(rb); err != nil {
			return c.JSON(http.StatusBadRequest, err)
		}
		reviewModel := model.Review{
			ID:                uint(idUint),
			Review:            rb.Review,
			ConcurrentControl: rb.ConcurrentControl,
		}
		res, err := h.usecase.EditReviewByID(reviewModel.ID, reviewModel)
		if err != nil {
			if err.Error() == constant.ErrUpdateConflict {
				return c.JSON(http.StatusBadRequest, model.BaseResponse{
					Msg: err.Error(),
					Err: err.Error(),
				})
			}
			return c.JSON(http.StatusInternalServerError, err)
		}
		return c.JSON(http.StatusOK, res)
	}
	return c.JSON(http.StatusBadRequest, model.BaseResponse{
		Msg: "Required parameter id",
	})
}
