package route

import "github.com/labstack/echo"

// ReviewRouteInterface : route interface for initial api router
type ReviewRouteInterface interface {
	Initial(e *echo.Echo)
}
