package route

import (
	"lmwn-assignment/api/review/handler"

	"github.com/labstack/echo"
)

type reviewRoute struct {
	handler handler.ReviewHandlerInterface
}

// NewReviewRoute : create review route instance
func NewReviewRoute(h handler.ReviewHandlerInterface) ReviewRouteInterface {
	return &reviewRoute{handler: h}
}

func (r *reviewRoute) Initial(e *echo.Echo) {
	api := e.Group("/reviews")

	api.GET("", r.handler.SearchReviewByQuery)
	api.GET("/:id", r.handler.GetReviewByID)
	api.PUT("/:id", r.handler.EditReviewByID)
}
