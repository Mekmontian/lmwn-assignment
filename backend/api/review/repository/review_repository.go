package repository

import (
	"encoding/json"
	"errors"
	"lmwn-assignment/constant"
	"lmwn-assignment/datasource"
	"lmwn-assignment/elasticsearch"
	"lmwn-assignment/model"
	"log"
)

type reviewRepository struct {
	elasticsearch  elasticsearch.Interface
	repositoryName string
}

// NewReviewRepository : create review repository instance
func NewReviewRepository(ds datasource.Datasource) ReviewRepositoryInterface {
	var es elasticsearch.Interface
	if ds.Elasticsearch != nil {
		es = ds.Elasticsearch
	}
	return &reviewRepository{elasticsearch: es, repositoryName: "reviews"}
}

func (r *reviewRepository) GetReviewByID(ID uint) (model.Review, error) {
	res, err := r.elasticsearch.GetDocumentByID(r.repositoryName, ID)
	if err != nil {
		return model.Review{}, err
	}
	var review model.Review
	if res != nil {
		if err := json.Unmarshal(res.Source, &review); err != nil {
			return review, err
		}
		review.ConcurrentControl = res.ConcurrentControl
		return review, nil
	}
	return review, errors.New(constant.ErrNotFound)
}

func (r *reviewRepository) SearchReviewByQuery(query string) ([]model.Review, error) {
	rws := []model.Review{}
	rss, err := r.elasticsearch.GetDocumentByQuery(r.repositoryName, "review", query)
	if err != nil {
		return rws, err
	}
	for _, rs := range rss {
		var review model.Review
		if err := json.Unmarshal(rs.Source, &review); err != nil {
			log.Println("Review repository unmarshal error with", err)
		}
		review.Review = string(rs.Highlight)
		review.ConcurrentControl = rs.ConcurrentControl
		rws = append(rws, review)
	}
	return rws, nil
}

func (r *reviewRepository) EditReviewByID(ID uint, review model.Review) (model.Review, error) {
	var newReview model.Review
	data := model.UpdatePayload{}
	data.ConcurrentControl = review.ConcurrentControl
	data.Payload = &review
	res, err := r.elasticsearch.UpdateDocumentByID(r.repositoryName, ID, data)
	if err != nil {
		return newReview, err
	}
	if err := json.Unmarshal(res.Source, &newReview); err != nil {
		return newReview, err
	}
	newReview.ConcurrentControl = res.ConcurrentControl
	return newReview, nil
}
