package repository

import "lmwn-assignment/model"

// ReviewRepositoryInterface : repository interface for manage multiple datasource
type ReviewRepositoryInterface interface {
	GetReviewByID(ID uint) (model.Review, error)
	SearchReviewByQuery(query string) ([]model.Review, error)
	EditReviewByID(ID uint, review model.Review) (model.Review, error)
}
