package usecase

import (
	"lmwn-assignment/api/review/repository"
	"lmwn-assignment/constant"
	"lmwn-assignment/model"
	"log"
)

type reviewUsecase struct {
	repository repository.ReviewRepositoryInterface
}

// NewReviewUsecase : create review use case instance
func NewReviewUsecase(r repository.ReviewRepositoryInterface) ReviewUsecaseInterface {
	return &reviewUsecase{repository: r}
}

func (uc *reviewUsecase) GetReviewByID(ID uint) (model.Review, error) {
	return uc.repository.GetReviewByID(ID)
}

func (uc *reviewUsecase) SearchReviewByQuery(query string) ([]model.Review, error) {
	return uc.repository.SearchReviewByQuery(query)
}

func (uc *reviewUsecase) EditReviewByID(ID uint, review model.Review) (model.Review, error) {
	res, err := uc.repository.EditReviewByID(ID, review)
	if err != nil {
		// TODO : handle concurrent update conflict
		if err.Error() == constant.ErrUpdateConflict {
			log.Println(err.Error())
			newReview := review
			newReview.ConcurrentControl = nil
			newRes, err := uc.repository.EditReviewByID(ID, newReview)
			return newRes, err
		}
		return res, err
	}
	return res, err
}
