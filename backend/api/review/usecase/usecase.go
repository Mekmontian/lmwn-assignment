package usecase

import "lmwn-assignment/model"

// ReviewUsecaseInterface : usecase interface for business logic
type ReviewUsecaseInterface interface {
	GetReviewByID(ID uint) (model.Review, error)
	SearchReviewByQuery(query string) ([]model.Review, error)
	EditReviewByID(ID uint, review model.Review) (model.Review, error)
}
