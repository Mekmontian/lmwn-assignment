package review

import (
	"lmwn-assignment/api/review/handler"
	"lmwn-assignment/api/review/repository"
	"lmwn-assignment/api/review/usecase"
	"lmwn-assignment/datasource"
)

// ProvideRepository : return repository dependency
func ProvideRepository(ds datasource.Datasource) repository.ReviewRepositoryInterface {
	return repository.NewReviewRepository(ds)
}

// ProvideUsecase : return usecase dependency
func ProvideUsecase(ds datasource.Datasource) usecase.ReviewUsecaseInterface {
	return usecase.NewReviewUsecase(ProvideRepository(ds))
}

// ProvideHandler : return handler dependency
func ProvideHandler(ds datasource.Datasource) handler.ReviewHandlerInterface {
	return handler.NewReviewHandler(ProvideUsecase(ds))
}
