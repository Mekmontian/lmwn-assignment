package utility

import "strings"

// TrimSpaceQuery : trim overall space of query
func TrimSpaceQuery(text string) string {
	return strings.Trim(text, " ")
}
