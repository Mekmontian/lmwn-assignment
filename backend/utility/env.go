package utility

import (
	"os"

	"github.com/joho/godotenv"
)

// LoadEnv : load environment configuration for development, test and production
func LoadEnv() {
	var path string
	env := os.Getenv("ENV")

	switch env {
	case "production":
		return
	case "development":
		path = "./"
	default:
		return
	}

	envFile := path + ".env." + env
	godotenv.Load(envFile)
}
