package utility_test

import (
	"lmwn-assignment/utility"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTrimSpaceQuery(t *testing.T) {
	expect := ""
	actual := utility.TrimSpaceQuery("                ")
	assert.Equal(t, expect, actual)
}
