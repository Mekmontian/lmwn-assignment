package constant

// ErrNotFound :
const ErrNotFound string = "content not found"

// ErrUpdateConflict :
const ErrUpdateConflict string = "data update sequence conflict"
