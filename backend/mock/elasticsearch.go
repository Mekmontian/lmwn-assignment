package mock

import (
	_ElasticsearchInterface "lmwn-assignment/elasticsearch"
	"lmwn-assignment/model"
)

type mockElasticsearch struct {
}

// NewMockElasticsearch : create Elasticsearch mock instance
func NewMockElasticsearch() _ElasticsearchInterface.Interface {
	return &mockElasticsearch{}
}

func (db mockElasticsearch) GetDocumentByID(index string, ID uint) (*model.ElasticBasicPayload, error) {
	return &model.ElasticBasicPayload{}, nil
}

func (db mockElasticsearch) UpdateDocumentByID(index string, ID uint, data model.UpdatePayload) (*model.ElasticUpdateResponse, error) {
	return &model.ElasticUpdateResponse{}, nil
}

func (db mockElasticsearch) GetDocumentByQuery(index, field, query string) ([]model.QueryWithHighlight, error) {
	return []model.QueryWithHighlight{}, nil
}
