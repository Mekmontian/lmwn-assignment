package review

import (
	_ReviewUsecaseInterface "lmwn-assignment/api/review/usecase"
	"lmwn-assignment/model"
)

type mockUsecase struct {
}

// NewMockUsecase : create review use case mock instance
func NewMockUsecase() _ReviewUsecaseInterface.ReviewUsecaseInterface {
	return &mockUsecase{}
}

func (u *mockUsecase) GetReviewByID(ID uint) (model.Review, error) {
	return model.Review{
		ID:     1,
		Review: "delicious",
		ConcurrentControl: &model.ConcurrentControl{
			SeqNo:       1,
			PrimaryTerm: 1,
		},
	}, nil
}

func (u *mockUsecase) SearchReviewByQuery(query string) ([]model.Review, error) {
	return []model.Review{
		{
			ID:     1,
			Review: "<keyword>delicious</keyword>",
			ConcurrentControl: &model.ConcurrentControl{
				SeqNo:       1,
				PrimaryTerm: 1,
			},
		},
		{
			ID:     2,
			Review: "super <keyword>delicious</keyword>",
			ConcurrentControl: &model.ConcurrentControl{
				SeqNo:       1,
				PrimaryTerm: 1,
			},
		},
	}, nil
}

func (u *mockUsecase) EditReviewByID(ID uint, review model.Review) (model.Review, error) {
	return model.Review{
		ID:     1,
		Review: "new delicious",
		ConcurrentControl: &model.ConcurrentControl{
			SeqNo:       2,
			PrimaryTerm: 1,
		},
	}, nil
}
