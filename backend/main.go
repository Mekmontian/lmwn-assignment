package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"lmwn-assignment/api/healthcheck"
	"lmwn-assignment/api/review"
	_reviewRoute "lmwn-assignment/api/review/route"
	"lmwn-assignment/datasource"
	"lmwn-assignment/elasticsearch"
	"lmwn-assignment/migrate"
	"lmwn-assignment/utility"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	logger = middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format:           "${time_custom} method=${method}, uri=${uri}, status=${status}, origin=${header:origin}\n",
		CustomTimeFormat: "2006-01-02 15:04:05",
	})
	cors = middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{
			"*",
		},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderContentType,
		},
		AllowMethods: []string{
			echo.GET,
			echo.PUT,
			echo.OPTIONS,
		},
	})
	serverPort            string
	elasticsearchEndpoint string
)

func init() {
	// Load Environment
	utility.LoadEnv()

	serverPort = ":" + os.Getenv("SERVER_PORT")
	elasticsearchEndpoint = os.Getenv("ELASTICSEARCH_ENDPOINT")
}

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) > 0 {
		if argsWithoutProg[0] == "migrate" {
			migrate.Migration(elasticsearchEndpoint)
			return
		}
	}

	time.Sleep(1 * time.Second)

	e := echo.New()
	e.Use(logger)
	e.Use(cors)

	log.Println(elasticsearchEndpoint)

	// Dependency
	elasticsearch, err := elasticsearch.NewElasticsearch(elasticsearchEndpoint)
	if err != nil {
		log.Panic(err)
	}
	datasource := datasource.Datasource{
		Elasticsearch: elasticsearch,
	}

	// Route
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	healthcheck.NewHealthCheckRoute().Initial(e)
	_reviewRoute.NewReviewRoute(review.ProvideHandler(datasource)).Initial(e)

	// Listen
	e.Logger.Fatal(e.Start(serverPort))
}
