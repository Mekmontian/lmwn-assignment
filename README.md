# LINE MAN x Wongnai Assignment

This project is about web application that reviewing and searching of food that implement with Vue.js as frontend and Go as backend

## Run Project Using Docker Environment
```sh
docker-compose -f docker-compose.yml up -d
```

## API Spec
 - https://documenter.getpostman.com/view/4086708/TVYF8Jwy

## Site URL
 - Website: http://localhost:5555
 - API: http://localhost:8888
 - Elasticsearch: http://localhost:9200