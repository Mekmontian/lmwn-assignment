export default {
  getReviews: (state: any) => state.reviews,
  getReview: (state: any) => state.review,
  getLoading: (state: any) => state.isLoading,
};
