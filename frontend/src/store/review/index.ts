import mutations from './mutations';
import actions from './actions';
import getters from './getters';

const state = {
  reviews: [],
  review: {},
  isLoading: false,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
