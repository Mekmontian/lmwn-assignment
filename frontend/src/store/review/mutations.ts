export default {
  updateReviews: (state: any, payload: any) => {
    state.reviews = payload;
  },
  updateReview: (state: any, payload: any) => {
    state.review = payload;
  },
  updateLoading: (state: any, payload: boolean) => {
    state.isLoading = payload;
  },
};
