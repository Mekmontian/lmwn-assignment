import api from '@/constant/api';

interface actionOption {
  commit: any
}

interface updateOption {
  id: number
  data: any
}

export default {
  fetchReviewsByQuery: ({ commit }: actionOption, query: string) => {
    commit('updateLoading', true);
    api.backendAPI.get('reviews', {
      params: {
        query,
      },
    }).then((res: any) => {
      commit('updateReviews', res.data);
      commit('updateLoading', false);
    });
  },
  fetchReviewByID: ({ commit }: actionOption, id: number) => {
    commit('updateLoading', true);
    api.backendAPI.get(`reviews/${id}`).then((res: any) => {
      commit('updateReview', res.data);
      commit('updateLoading', false);
    });
  },
  updateReviewByID: ({ commit }: actionOption, { id, data }: updateOption) => new Promise(
    (resolve) => {
      commit('updateLoading', true);
      api.backendAPI.put(`reviews/${id}`, data).then((res: any) => {
        commit('updateReview', res.data);
        commit('updateLoading', false);
        resolve();
      });
    },
  ),
  clearReviews: ({ commit }: actionOption) => {
    commit('updateLoading', true);
    commit('updateReviews', []);
    commit('updateLoading', false);
  },
};
