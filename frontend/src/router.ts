import Vue from 'vue';
import Router from 'vue-router';
import Review from '@/views/Review.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue'),
    },
    {
      path: '/reviews',
      name: 'reviews',
      component: Review,
    },
    {
      path: '/reviews/:id',
      name: 'edit-reviews',
      component: () => import(/* webpackChunkName: "about" */ './views/EditReview.vue'),
    },
  ],
});
