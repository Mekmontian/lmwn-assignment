import axios from 'axios';

const backendEndpoint = process.env.VUE_APP_API_ENDPOINT;

const backendAPI = axios.create({
  baseURL: backendEndpoint,
});

export default {
  backendAPI,
};
